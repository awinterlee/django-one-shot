from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm

# Create your views here.
def todo_list_list(request):
    todolist = TodoList.objects.all()



    context = {
        'todo_list_list': todolist,
    }
    return render(request, 'todos/todolist.html', context)

def todo_list_detail(request, id):
    tododetail = get_object_or_404(TodoList, id=id)
    context = {
        'tododetail_object': tododetail,
    }
    return render(request, 'todos/tododetail.html', context)

def todo_list_create(request):
    if request.method == 'POST':
        form = TodoForm(request.POST)

        if form.is_valid():
            todo_list = form.save()
            return redirect('todo_list_detail', id = todo_list.id)

    else:
        form = TodoForm()

    context = {'form': form}

    return render(request, 'todos/todocreate.html', context)

def todo_list_update(request, id):
    todoedit = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoForm(request.POST, instance=todoedit)

        if form.is_valid():
            todo_detail = form.save()
            return redirect('todo_list_detail', id=todo_detail.id)

    else: form = TodoForm(instance=todoedit)

    context = {'form': form}
    return render(request, 'todos/todoedit.html', context)

def todo_list_delete(request, id):
    tododelete = get_object_or_404(TodoList, id=id)

    if request.method == 'POST':
        tododelete.delete()
        return redirect('todo_list_list')

    return render(request, 'todos/tododelete.html')
